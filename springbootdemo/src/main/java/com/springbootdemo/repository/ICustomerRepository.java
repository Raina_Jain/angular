package com.springbootdemo.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.model.Customer;
public interface ICustomerRepository extends CrudRepository<Customer,Long> {
	List<Customer>findByAge(Integer age);

}
